------------------------------------------------------版本1.2--------------------------------------------------------

mvp中的rxjava请求，实现队列请求

------------------------------------------------------版本1.1--------------------------------------------------------
   1.请求优化，去除多余的类
   2.mvp优化  修改的插件（谢谢MVPHelper作者） 在assets里 MVPHelper_contract.jar
   (最近自己开发项目越来越大，接口越来越多，发现model Impl 类太多,找起来也麻烦，所以把M层和V层放在了Contract包中，一个类搞定，p层还是单独放，自动生成，并且初始化)





   如果不使用mvp 不推荐用IRequestRx 直接请求，会失去noHttp的特性，例如请队列等

           String mUrl = "http://image.baidu.com/channel/listjson?pn=" + 1
                   + "&rn=" + 22
                   + "&tag1=%E6%98%8E%E6%98%9F&tag2=%E5%85%A8%E9%83%A8";

           IRequest.get(this, mUrl)
                   .execute(new RequestListener<String>() {
                       @Override
                       public void onSuccess(Response<String> result) {
                       }
                   });
           IRequest.get(this, mUrl)
                   .params("", "")
                   .loading(true)
                   .execute(new RequestListener<String>() {
                       @Override
                       public void onSuccess(Response<String> result) {

                       }
                       @Override
                       public void onFailed(int what, Response result) {
                           super.onFailed(what, result);
                       }
                   });











------------------------------------------------------版本1.0--------------------------------------------------------
  一直想尝试用mvp-rxjava-nohttp

  看了不少别人写的mvp的demo，例如T-mvp等 各有各的好，找到适合自己的就行

  还好有个大牛出了MVPHelper插件,不然打死我也不会用mvp,因为写的类太多。为了避免model类重名，我就把MVPHelper里面小改了下，把LoginModel 改成ILoginModel，不喜欢可以忽略

  看了Author: Othershe写的mvp  就抽走了几个mvp类


   我写mvp流程如果下：（仅参考）
   1.写view层的回调
   2.在写model rx回调
   3.在写 实现model层 （业务写在这里）
   4.然后再写p层






        Map<String,String> params = new HashMap<>();
        IRequest.post(this, mUrl, params)
                .loading(true)
                .execute(PuBuLiuModel.class, new RequestArrayListener<PuBuLiuModel>() {
                    @Override
                    public void onSuccess(Response<PuBuLiuModel> result) {

                        Toast.makeText(MainActivity.this, JsonUtils.string(result.get()), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailed(int what, Response result) {
                        super.onFailed(what, result);
                    }
                });

        IRequest.get(this, mUrl)
                .loading(true)
                .loadingTitle("努力中...")
                .execute(new RequestListener() {
                    @Override
                    public void onSuccess(Response<String> result) {
                        Toast.makeText(MainActivity.this, "获取成功", Toast.LENGTH_SHORT).show();
                    }
                });
        IRequestRx.get(this,mUrl)
                .execute(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(String s) {

                    }
                });

        IRequestRx.get(this,mUrl)
                .execute(PuBuLiuModel.class, new Subscriber<PuBuLiuModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(PuBuLiuModel puBuLiuModel) {

                    }
                });
